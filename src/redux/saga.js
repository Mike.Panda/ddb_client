import { all } from "redux-saga/effects"
import { saga as navBar } from "../ducs/NavBar"
import { saga as app } from "../ducs/Wrapper"

export default function* () {
  yield all([navBar(), app()])
}
