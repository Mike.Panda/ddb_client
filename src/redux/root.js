import { combineReducers } from "redux"
import { persistReducer } from "redux-persist"
import storage from "redux-persist/lib/storage"
import { Reducer as tables } from "../ducs/NavBar"
import { Reducer as app } from "../ducs/Wrapper"

export const root = combineReducers({
  tables: tables,
  app: persistReducer({ key: "app", storage }, app),
})
