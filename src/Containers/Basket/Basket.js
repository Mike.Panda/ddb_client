import React from "react"
import { connect } from "react-redux"
import { buyGoods } from "../../ducs/Wrapper"
import "./Basket.scss"

const total = (baskset) => {
  let price = 0
  baskset.map((item) => {
    price += item.count * item.price
  })
  return price
}

const Basket = ({ buyGoods, basket }) => {
  return (
    <div className='Basket_Wrapper'>
      <div className='Basket_Content'>
        <div className='Basket_Content_Header'>
          <p>Item</p>
          <p>Count</p>
          <p>Price</p>
          <p>Total</p>
        </div>
        {basket.map((item) => (
          <div className='Basket_Content_Order'>
            <p>{item.name}</p>
            <p>{item.count}</p>
            <p>{item.price}$</p>
            <p>{item.count * item.price}$</p>
          </div>
        ))}
        <div className='Basket_Content_Total'>
          <p>
            Total: <span>{total(basket)}$</span>
          </p>
          <button onClick={buyGoods}>BUY</button>
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = ({ app: { basket } }) => {
  return { basket }
}

export default connect(mapStateToProps, { buyGoods })(Basket)
