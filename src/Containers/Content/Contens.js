import React from "react"
import { connect } from "react-redux"
import Table from "./Table"
import userIcon from "../../image/user.svg"
import shopIcon from "../../image/shop.svg"
import { showAuthWindow, showBasket } from "../../ducs/Wrapper"
import { names } from "../NavBar/constants"

import "./Content.scss"

const Content = ({
  data,
  tableName,
  showAuthWindow,
  authorizationShow,
  user,
  basket,
  showBasket,
}) => {
  return (
    <div className='Content_Wrapper'>
      <div className='Content_Wrapper_Header'>
        <h1>{names[tableName]}</h1>
        <div>
          <div onClick={() => showAuthWindow(!authorizationShow)}>
            <p>{user ? user : "Login"}</p>
            <img src={userIcon} />
          </div>
          <div onClick={() => showBasket()}>
            <img src={shopIcon} />
            <p>{basket.length}</p>
          </div>
        </div>
      </div>
      <Table />
    </div>
  )
}

const mapStateToProps = ({
  tables: { tableName },
  app: { authorizationShow, user, basket },
}) => ({
  tableName,
  authorizationShow,
  user,
  basket,
})

const mapDispatchToProps = {
  showAuthWindow,
  showBasket,
}

export default connect(mapStateToProps, mapDispatchToProps)(Content)
