import React, { useState, useEffect } from "react"
import { connect } from "react-redux"
import { addData, deleteData, updateData } from "../../../actions/database"
import { names } from "./constants"
import { updateBasket } from "../../../ducs/Wrapper"

import binIcon from "../../../image/bin.svg"
import penIcon from "../../../image/pen.svg"
import plusIcon from "../../../image/plus.svg"

import "./Table.scss"

const Table = ({ data, tableName, role, updateBasket }) => {
  const [fieldsTable, setFieldsTable] = useState([])
  const [dataTable, setDataTable] = useState(data)
  const [editFlag, setEditFlag] = useState(-1)
  const [basket, setBasket] = useState([])

  useEffect(() => {
    var input = document.querySelectorAll("input")
    input.forEach((item) => {
      if (item.getAttribute("type") === "text") {
        item.setAttribute("size", item.getAttribute("placeholder").length)
      }
    })
  }, [dataTable, editFlag])

  useEffect(() => {
    if (data[0]) {
      const fields = Object.keys(data[0]).map((item) => {
        return item
      })
      setFieldsTable(fields)
      setDataTable(data)
    }
  }, [data])

  useEffect(() => {
    console.log(basket)
    updateBasket(basket)
  }, [basket])

  const basketPlus = (product) => {
    let isFind = false
    const basketNew = [...basket]
    basketNew.map((item) => {
      if (item.id === product.id_goods) {
        item.count++
        setBasket(basketNew)
        isFind = true
      }
    })
    !isFind &&
      setBasket(
        basket.concat({
          id: product.id_goods,
          name: product.name_goods,
          count: 1,
          price: product.price_1_unit,
        })
      )
  }

  const basketMinus = (product) => {
    const basketNew = [...basket]
    basketNew.map((item, index) => {
      if (item.id === product.id_goods) {
        item.count--
        if (item.count === 0) {
          basketNew.splice(index, 1)
        }
        setBasket(basketNew)
      }
    })
  }

  const findInBasket = (id) => {
    let count = 0
    basket.map((item) => {
      if (item.id === id) {
        count = item.count
      }
    })
    return count
  }

  const handleKeyDown = async (e) => {
    const sendData = {}
    if (e.key === "Enter") {
      fieldsTable.forEach((item) => {
        sendData[item] = document.getElementById(item).value
      })
      const result = await addData({ data: sendData, tableName: tableName })
      if (result.effect) {
        const dataTableFS = [...dataTable]
        dataTableFS.push(sendData)
        setDataTable(dataTableFS)
        var input = document.querySelectorAll("input")
        input.forEach((item) => {
          if (item.getAttribute("type") === "text") {
            item.value = ""
          }
        })
      }
    }
  }

  const deleteRow = async (row, index) => {
    const result = await deleteData({ data: row, tableName: tableName })
    if (result.effect) {
      const dataTableFS = [...dataTable]
      dataTableFS.splice(index, 1)
      setDataTable(dataTableFS)
    }
  }

  const editRow = async (row, index) => {
    setEditFlag(index)
    setTimeout(() => {
      Object.keys(row).forEach((item) => {
        const input = document.getElementById("edit_" + item)
        input.value = row[item]
      })
    }, 0)
  }

  const sendEditData = async (e, row, index) => {
    const sendData = {}
    if (e.key === "Enter") {
      Object.keys(row).forEach((item) => {
        const input = document.getElementById("edit_" + item)
        sendData[item] = input.value
      })
      const result = await updateData({ data: sendData, tableName: tableName })
      if (result.effect) {
        const dataTableFS = [...dataTable]
        dataTableFS[index] = sendData
        setEditFlag(-1)
        setDataTable(dataTableFS)
      }
    }
  }

  return (
    <div className='Table_Wrapper'>
      <table>
        <tr>
          {dataTable[0] &&
            Object.keys(dataTable[0]).map((item) => {
              return <th>{names[item] || item}</th>
            })}
        </tr>
        {dataTable &&
          dataTable.map((row, index) => {
            return (
              <tr>
                {editFlag === index
                  ? Object.keys(row).map((item) => {
                      return (
                        <td>
                          <input
                            id={"edit_" + item}
                            type='text'
                            placeholder={row[item]}
                            onKeyDown={(event) => {
                              sendEditData(event, row, index)
                            }}
                          />
                        </td>
                      )
                    })
                  : Object.keys(row).map((item) => {
                      return <td style={{ padding: "15px" }}>{row[item]}</td>
                    })}
                {role === "admin" && (
                  <div>
                    <img
                      onClick={() => deleteRow(row, index)}
                      src={binIcon}
                      alt='bin icon'
                    />
                    {editFlag !== index && (
                      <img
                        onClick={() => editRow(row, index)}
                        src={penIcon}
                        alt='pen icon'
                      />
                    )}
                  </div>
                )}
                {tableName === "goods" && role === "seller" && (
                  <div className='Table_Buttons'>
                    <span onClick={() => basketMinus(row)}>-</span>
                    <p>{findInBasket(row.id_goods)}</p>
                    <span onClick={() => basketPlus(row)}>+</span>
                  </div>
                )}
              </tr>
            )
          })}
        {role === "admin" && (
          <tr className='Table_Inputs'>
            {dataTable[0] &&
              Object.keys(dataTable[0]).map((item) => {
                return (
                  <td>
                    <input
                      id={item}
                      type='text'
                      placeholder={item}
                      onKeyDown={(event) => {
                        handleKeyDown(event)
                      }}
                    />
                  </td>
                )
              })}
          </tr>
        )}
      </table>
    </div>
  )
}

const mapStateToProps = ({
  tables: { tableData, tableName },
  app: { role },
}) => ({
  data: tableData,
  tableName,
  role,
})

const mapDispatchToProps = { updateBasket }

export default connect(mapStateToProps, mapDispatchToProps)(Table)
