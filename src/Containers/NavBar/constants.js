export const names = {
  category_goods: "Product category",
  goods: "Goods",
  manufacturer: "Manufacturer",
  orders: "Orders",
  users: "Users",
}
