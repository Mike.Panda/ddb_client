import React, { useEffect, useState } from "react"
import { connect } from "react-redux"
import { getTablesName, getTableData } from "../../ducs/NavBar"
import { changeFullDBFlag } from "../../ducs/Wrapper"
import { names } from "./constants"

import "./NavBar.scss"

const NavBar = ({
  tablesName,
  getTablesName,
  getTableData,
  changeFullDBFlag,
  fullDBFlag,
  role,
}) => {
  useEffect(() => {
    getTablesName()
  }, [])

  const getData = (name) => {
    getTableData(name)
  }

  return role ? (
    <div className='NavBar_Wrapper'>
      <ul>
        {tablesName.length &&
          tablesName.map((element) => {
            return (
              <li
                onClick={() => {
                  getData(element.Tables_in_organization)
                }}
              >
                {console.log(names, element.Tables_in_organization)}
                {names[element.Tables_in_organization]}
              </li>
            )
          })}
      </ul>
      <div className='Toggle'>
        <p
          onClick={() => {
            changeFullDBFlag(!fullDBFlag)
          }}
        >
          {fullDBFlag ? "Full DataBase" : "Local Database"}
        </p>
      </div>
    </div>
  ) : (
    <div></div>
  )
}

const mapStateToProps = ({
  tables: { tablesName },
  app: { fullDBFlag, role },
}) => ({
  tablesName,
  fullDBFlag,
  role,
})

const mapDispatchToProps = {
  getTablesName,
  getTableData,
  changeFullDBFlag,
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar)
