import React, { useState } from "react"
import { connect } from "react-redux"
import { AuthorizationRequest, RegistrationRequest } from "../../ducs/Wrapper"
import "./Authorization.scss"

const Authorization = ({ RegistrationRequest, AuthorizationRequest }) => {
  const sendAuthorization = () => {
    const email = document.getElementById("email").value
    const password = document.getElementById("password").value
    AuthorizationRequest({ email: email, password: password })
  }

  const sendRegistration = () => {
    if (
      document.getElementById("password_reg").value ===
      document.getElementById("password_again_reg").value
    ) {
      const email = document.getElementById("email_reg").value
      const password = document.getElementById("password_reg").value
      RegistrationRequest({ email: email, password: password })
    }
  }

  const [action, setAction] = useState("authorization")
  return action === "authorization" ? (
    <div className='Authorization_Wrapper'>
      <div>
        <h3>Authorization</h3>
        <div>
          <label htmlFor='email'>Email</label>
          <input type='text' name='email' id='email' placeholder='email' />
          <label htmlFor='password'>Password</label>
          <input
            type='password'
            name='password'
            id='password'
            placeholder='password'
          />
        </div>
        <button onClick={sendAuthorization}>Login</button>
        <p onClick={() => setAction("registration")}>Forgot your password?</p>
      </div>
    </div>
  ) : (
    <div className='Authorization_Wrapper'>
      <div>
        <h3>Authorization</h3>
        <div>
          <label htmlFor='email'>Email</label>
          <input
            type='text'
            name='email_reg'
            id='email_reg'
            placeholder='email'
          />
          <label htmlFor='password'>Password</label>
          <input
            type='password'
            name='password_reg'
            id='password_reg'
            placeholder='password'
          />
          <label htmlFor='password'>Repeat password</label>
          <input
            type='password'
            name='password_again_reg'
            id='password_again_reg'
            placeholder='password'
          />
        </div>
        <button onClick={sendRegistration}>Registration</button>
      </div>
    </div>
  )
}

const mapStateToProps = ({ app: { authorizationShow } }) => {
  return { authorizationShow: authorizationShow }
}

const mapDispatchToProps = {
  AuthorizationRequest,
  RegistrationRequest,
}

export default connect(mapStateToProps, mapDispatchToProps)(Authorization)
