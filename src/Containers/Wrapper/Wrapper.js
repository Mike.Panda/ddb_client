import React, { useState } from "react"
import { connect } from "react-redux"

import Navbar from "../NavBar"
import Content from "../Content"
import Authorization from "../Authorization"
import Basket from "../Basket"

import "./Wrapper.scss"

const Wrapper = ({ authorizationShow, basketShow }) => {
  return (
    <div className='Wrapper'>
      <Navbar />
      <Content />
      {authorizationShow && <Authorization />}
      {basketShow && <Basket />}
    </div>
  )
}

const mapStateToProps = ({ app: { authorizationShow, basketShow } }) => ({
  authorizationShow,
  basketShow,
})

export default connect(mapStateToProps, null)(Wrapper)
