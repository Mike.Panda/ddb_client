import { useSelector } from "react-redux"
import store from "../index"

const header = {
  headers: {
    "Content-Type": "application/json",
  },
}

export const getTables = async () => {
  const response = await fetch("http://127.0.0.1:8000/getTables", header)

  if (response.ok) {
    const json = await response.json()
    return json
  }
}

export const getDataTable = async (name) => {
  const database = store.getState().app.dataBaseName
  const response = await fetch(
    `http://127.0.0.1:8000/getData/${name}/${database}`,
    header
  )

  if (response.ok) {
    const json = await response.json()
    return json
    // return Promise.resolve(json)
  }
}

export const getUserData = async (name, password) => {
  console.log(name, password)
  const response = await fetch(
    `http://127.0.0.1:8000/getRole/${name}/${password}`,
    header
  )

  if (response.ok) {
    const json = await response.json()
    return json
    // return Promise.resolve(json)
  }
}

export const getFullDataTable = async (name) => {
  const response = await fetch(
    `http://127.0.0.1:8000/getFullData/${name}`,
    header
  )

  if (response.ok) {
    const json = await response.json()
    return json
  }
}

export const addData = async (data) => {
  const database = store.getState().app.dataBaseName
  const response = await fetch(`http://127.0.0.1:8000/addData/${database}`, {
    method: "POST",
    ...header,
    body: JSON.stringify(data),
  })

  if (response.ok) {
    const json = await response.json()
    return json
  }
}

export const deleteData = async (data) => {
  const database = store.getState().app.dataBaseName
  const response = await fetch(`http://127.0.0.1:8000/deleteData/${database}`, {
    method: "POST",
    ...header,
    body: JSON.stringify(data),
  })

  if (response.ok) {
    const json = await response.json()
    return json
  }
}

export const updateData = async (data) => {
  const database = store.getState().app.dataBaseName
  const response = await fetch(`http://127.0.0.1:8000/updateData/${database}`, {
    method: "POST",
    ...header,
    body: JSON.stringify(data),
  })

  if (response.ok) {
    const json = await response.json()
    return json
  }
}
