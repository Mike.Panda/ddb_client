import { select, all, takeEvery, call, put } from "redux-saga/effects"
import { getTables, getDataTable, getFullDataTable } from "../actions/database"

//helpers
const initialState = {
  tablesName: [],
  tableData: [],
  tableName: "",
}

//types

const GET_TABLES_NAME_REQUEST = "navBar/GET_TABLES_NAME_REQUEST"
const GET_TABLES_NAME_SAVE = "navBar/GET_TABLES_NAME_SAVE"
const GET_DATA_TABLE_REQUEST = "navBar/GET_DATA_TABLE_REQUEST"
const GET_DATA_TABLE_SAVE = "navBar/GET_DATA_TABLE_SAVE"

//actions

export const getTablesName = () => ({
  type: GET_TABLES_NAME_REQUEST,
})

export const saveTablesName = (payload) => ({
  type: GET_TABLES_NAME_SAVE,
  payload: payload,
})

export const getTableData = (name) => ({
  type: GET_DATA_TABLE_REQUEST,
  name: name,
})

export const saveTableData = (name, data) => ({
  type: GET_DATA_TABLE_SAVE,
  payload: { name: name, data: data },
})

//reducer

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TABLES_NAME_SAVE:
      return { ...state, tablesName: action.payload }
    case GET_DATA_TABLE_SAVE:
      return {
        ...state,
        tableData: action.payload.data,
        tableName: action.payload.name,
      }
    default:
      return state
  }
}

//saga

export function* saga() {
  yield takeEvery(GET_TABLES_NAME_REQUEST, getTablesNameSaga)
  yield takeEvery(GET_DATA_TABLE_REQUEST, getDataTableSaga)
}

function* getTablesNameSaga() {
  const tables = yield call(getTables)
  yield put(saveTablesName(tables))
}

function* getDataTableSaga(action) {
  const {
    app: { fullDBFlag },
  } = yield select()
  if (fullDBFlag) {
    const dataFullTable = yield call(getFullDataTable, action.name)
    yield put(saveTableData(action.name, dataFullTable.flat()))
  } else {
    const dataTable = yield call(getDataTable, action.name)
    yield put(saveTableData(action.name, dataTable))
  }
}
