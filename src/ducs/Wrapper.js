import { all, takeEvery, call, put } from "redux-saga/effects"
import { createStore } from "redux"
import { getUserData } from "../actions/database"

//helpers

const initialState = {
  fullDBFlag: false,
  authorizationShow: false,
  basketShow: false,
  user: "",
  role: "",
  dataBaseName: "",
  basket: [],
}
// //types

const CHANGE_FULL_DB_FLAG = "wrapper/CHANGE_FULL_DB_FLAG"
const CHANGE_AUTHORIZATION_FLAG = "wrapper/CHANGE_AUTHORIZATION_FLAG"
const AUTHORIZATION_REQUEST = "wrapper/AUTHORiZATION_REQUEST"
const AUTHORIZATION_SAVE = "wrapper/AUTHORiZATION_SAVE"
const REGISTRATION_REQUEST = "wrapper/REGISTRATION_REQUEST"
const REGISTRATION_SAVE = "wrapper/REGISTRATION_SAVE"
const UPDATE_BASKET = "wrapper/UPDATE_BASKET"
const SHOW_BASKET = "wrapper/SHOW_BASKET"
const BUY_GOODS = "wrapper/BUY_GOODS"

// //actions

export const changeFullDBFlag = (flag) => ({
  type: CHANGE_FULL_DB_FLAG,
  payload: flag,
})

export const showAuthWindow = (flag) => ({
  type: CHANGE_AUTHORIZATION_FLAG,
  payload: flag,
})

export const AuthorizationRequest = (payload) => ({
  type: AUTHORIZATION_REQUEST,
  payload: payload,
})

export const AuthorizationSave = (payload) => ({
  type: AUTHORIZATION_SAVE,
  payload: payload,
})

export const RegistrationRequest = (payload) => ({
  type: REGISTRATION_REQUEST,
  payload: payload,
})

export const RegistrationSave = (payload) => ({
  type: REGISTRATION_SAVE,
  payload: payload,
})

export const updateBasket = (payload) => ({
  type: UPDATE_BASKET,
  payload: payload,
})

export const showBasket = () => ({
  type: SHOW_BASKET,
})

export const buyGoods = () => ({
  type: BUY_GOODS,
})

// export const saveTablesName = payload => ({
//   type: GET_TABLES_NAME_SAVE,
//   payload: payload,
// })

// //reducer

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_BASKET:
      return {
        ...state,
        basket: action.payload,
      }
    case CHANGE_FULL_DB_FLAG:
      return { ...state, fullDBFlag: action.payload }
    case CHANGE_AUTHORIZATION_FLAG:
      return { ...state, authorizationShow: action.payload }
    case AUTHORIZATION_SAVE:
      return {
        ...state,
        user: action.payload.login,
        role: action.payload.role,
        dataBaseName: action.payload.db,
        authorizationShow: false,
      }
    case SHOW_BASKET:
      return {
        ...state,
        basketShow: true,
      }
    case BUY_GOODS:
      return {
        ...state,
        basket: [],
        basketShow: false,
      }
    default:
      return state
  }
}

export const store = createStore(Reducer)

// //saga

export function* saga() {
  yield takeEvery(AUTHORIZATION_REQUEST, AuthorizationRequestSaga)
}

function* AuthorizationRequestSaga(action) {
  yield call(() => {
    console.log("=======", action)
  })
  const user = yield call(
    getUserData,
    action.payload.email,
    action.payload.password
  )

  yield put(AuthorizationSave(user[0]))
}
