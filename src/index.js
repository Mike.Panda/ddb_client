import React from "react"
import ReactDOM from "react-dom"
import { createStore, compose, applyMiddleware } from "redux"
import createSagaMiddleware from "redux-saga"
import { PersistGate } from "redux-persist/integration/react"
import { persistStore } from "redux-persist"
import { root } from "./redux/root"
import { Provider } from "react-redux"
import "./index.scss"
import App from "./App"
import * as serviceWorker from "./serviceWorker"
import saga from "./redux/saga"

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  root,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)
sagaMiddleware.run(saga)
const persistor = persistStore(store)

const app = (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>
)

ReactDOM.render(app, document.getElementById("root"))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()

export default store
